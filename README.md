Mobile testing helper for Cypress
=================================

![book page swipe](https://gitlab.com/nTopus/cy-mobile-commands/uploads/7d4b42ad1e4ae970079386aedde01eb7/book.gif)![map pan and zoom](https://gitlab.com/nTopus/cy-mobile-commands/uploads/8142ff0818f4326032c722c7b5348029/map.gif)

## Installing

### Step 1, intall this package

```bash
npm install --save-dev cy-mobile-commands
```

### Step 2, load it to your Cypress test context

Open `cypress/support/index.js` and add:
```javascript
import 'cy-mobile-commands'
```

### Step 3, ...

_there is no more steps._

## Commands

### `swipe`

#### Syntax

```javascript
.swipe(checkpoint1, checkpoint2[, ..., checkpointN])
.swipe(configObject, checkpoint1, checkpoint2[, ..., checkpointN])
```

The `configObject` parameter is optional. The available options are:
* `delay`: (number of milliseconds = 300) the delta time from the `touchstart` to `touchend`.
* `steps`: (integer = computed) the number of steps between two checkpoints.
* `draw`: (boolean = true) display the swipe path over the page.

You can set two or more steps to make the swipe path as complex as you need.

Where `checkpoint#` can be a position, or an array of positions. An array of positions perform a multi touch action.

Where position can be:
* A explicit position defined with number values: `[clientX, clientY]`.
* A named position: `left`, `right`, `top`, `bottom`, `top-left`, `top-right`, `bottom-left`, `bottom-right` or `center`. (You can replace `kebab-case` by `camelCase`)

### `visitMobile`

#### Syntax

```javascript
cy.visitMobile(url)
cy.visitMobile(url, options)
cy.visitMobile(options)
```

It is exactly like the `cy.visit` command, extended with some env configuration to make mobile friendly libs to believe it is a mobile env.

For now it is necessary for [swipe.js](https://swipe.js.org) and [leaflet.js](https://leafletjs.com).
If you discover a lib that wont work with `cy.visit` or `cy.visitMobile`, please, [send us a issue](https://gitlab.com/nTopus/cy-mobile-commands/issues).

**💡 Tip:** If your lib expects a mobile env not yet provided by `cy.visitMobile`, run `cypress open` and with the visible browser GUI, open the console (F12) and click in "Toggle device toolbar" icon (or press `Ctrl+Shift+M`). Then you can run the test again as it was in a mobile chrome.

### Usage example

```javascript
it('page switch', () => {
  cy.visit('book.somewhere')
  cy.get('#my-page1').should('be.visible')
  cy.get('#my-page2').should('not.be.visible')
  cy.get('#my-slidable-book').swipe('right', 'left')
  cy.get('#my-page1').should('not.be.visible')
  cy.get('#my-page2').should('be.visible')
})

it('zoom a map', () => {
  cy.visitMobile('map.somewhere')
  cy.get('#map').swipe({delay: 2000}, [[80,250],[80,350]], [[80,100],[80,500]])
})
```

[For more usage examples, see the our tests.](https://gitlab.com/nTopus/cy-mobile-commands/tree/master/cypress/integration)
